import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bluelinelabs.conductor.Controller
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import org.greeting.PresentationControllerImpl
import library.UiBinder
import org.greeting.sample.MainEvents
import org.greeting.sample.MainItems
import org.greeting.sample.MainPresenter
import org.konan.multiplatform.R
import org.konan.multiplatform.bind

class MainBinder(view: RecyclerView) :
        UiBinder<List<MainItems>> {
    val adapter = GroupAdapter<ViewHolder>()
    init {
        view.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        view.adapter = adapter
    }

    override fun bind(model: List<MainItems>, oldModel: List<MainItems>?) {
        model.map {
            when (it) {
                is MainItems.SimpleText -> FirstItem(it)
                is MainItems.ClickableItem -> SecondItem(it)
                is MainItems.Image -> ThirdItem(it)
            }
        }.let {
            adapter.updateAsync(it)
        }
    }
}

class MainController() : Controller() {
    init { PresentationControllerImpl(MainPresenter(),::MainBinder).bind(this) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View = inflater.inflate(R.layout.activity_m, container, false)

}


