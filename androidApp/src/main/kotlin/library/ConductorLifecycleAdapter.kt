package org.konan.multiplatform

import android.view.View
import com.bluelinelabs.conductor.Controller
import org.greeting.PresentationControllerImpl


fun <StateT:Any,ViewModelT:Any,EventT:Any,ViewT:Any> PresentationControllerImpl<StateT,ViewModelT,EventT,ViewT>.bind(controller: Controller):PresentationControllerImpl<StateT,ViewModelT,EventT,ViewT> {
    controller.addLifecycleListener(object : Controller.LifecycleListener() {
        override fun postAttach(controller: Controller, view: View) {
            this@bind.onStart()
        }

        override fun postDetach(controller: Controller, view: View) {
            this@bind.onStop()
        }

        override fun postCreateView(controller: Controller, view: View) {
            this@bind.onCreateView(view as ViewT)
        }

        override fun preDestroyView(controller: Controller, view: View) {
            this@bind.onDestroyView()
        }

        override fun preDestroy(controller: Controller) {
            this@bind.onDestroy()
        }
    })
    return this
}