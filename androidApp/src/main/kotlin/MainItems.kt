import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import org.greeting.sample.MainItems
import org.konan.multiplatform.R

data class FirstItem(private val thirdItem: MainItems.SimpleText) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        (viewHolder.containerView as TextView).text = thirdItem.text
    }

    override fun getLayout(): Int = R.layout.first

    override fun getId(): Long = thirdItem.id
}

data class SecondItem(private val clickableItemItem: MainItems.ClickableItem) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        (viewHolder.containerView as TextView).text = clickableItemItem.header
        (viewHolder.containerView as TextView).setOnClickListener { clickableItemItem.onClick() }
    }

    override fun unbind(holder: ViewHolder) {
        super.unbind(holder)
        holder.containerView.setOnClickListener(null)
    }

    override fun getLayout(): Int = R.layout.first

    override fun getId(): Long = clickableItemItem.id


}

data class ThirdItem(private val state: MainItems.Image): Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        Picasso.get().load(state.image.url).placeholder(R.drawable.abc_vector_test).into(viewHolder
                .containerView as
                ImageView)
    }

    override fun getLayout(): Int = R.layout.third


    override fun equals(other: Any?): Boolean =
            state.image.url == (other as? ThirdItem)?.state?.image?.url

    override fun getId(): Long = state.id

}