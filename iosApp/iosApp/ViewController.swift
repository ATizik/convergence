//
//  ViewController.swift
//  iosApp
//
//  Created by jetbrains on 12/04/2018.
//  Copyright © 2018 JetBrains. All rights reserved.
//

import UIKit
import greeting

class MainBinde: UiBinder {
    
    init(view: UIView) {
        
    }
    
    func bind(model: Any, oldModel: Any?, view: Any) {
        (model as! Array<MainItems>).map { (item) -> Any in
            
            switch item {
                case is MainItems.: break
                case is MainItems.Second: break
                case is MainItems.Third: break
            }
            
            
        }
    }
    
    var offerEvent: (Any) -> KotlinUnit
    
    
}





class ViewController: UIViewController {
    required init?(coder aDecoder: NSCoder) {
        PresentationController.init(presenter: presenter, binder: MainBinde.init) {
            
        }
    }
    let presenter = MainPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let product = Factory().create(config: ["user": "JetBrains"])
        label.text = product.description
        
        


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var label: UILabel!
}

