package sample

import android.app.Application
import com.database.DbImpl
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver

private lateinit var application: Application
/**
 *  Call from [Application.onCreate] to do various
 *  initializations that rely on application context
 */
fun initLibrary(app: Application) {
    application = app
    Database
}
actual val driver: SqlDriver by lazy { AndroidSqliteDriver(DbImpl.Schema, application.applicationContext, "app.db") }
actual val Database: DbImpl by lazy { createQueryWrapper(driver) }