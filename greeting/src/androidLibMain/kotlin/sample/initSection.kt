package org.greeting.sample


actual fun MainPresenter.initSection(): List<MainItems> =
        listOf(
                MainItems.SimpleText("Kotlin, this is!", 11),
                MainItems.ClickableItem("Clickity-click me to load repositories!", 12) {
                    offerEvent(MainEvents.OnLoadingClicked)
                },
                MainItems.SimpleText("Take note that this view is Android specific - expect/actual in action", 13),
                MainItems.Image(ImageUrl("https://cdn.iconscout.com/icon/free/png-256/kotlin-283155.png"), 13)
        )