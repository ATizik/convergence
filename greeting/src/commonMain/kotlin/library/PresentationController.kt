package org.greeting

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import library.UiBinder

/**
 *  [onCreateView] is called once per view lifecycle, [UiBinder] will do additional init here
 *  [onStart] and [onStop] can be called one after the other any arbitrary number of times
 *  While this won't necessarily be true on all platforms and in all configurations,
 *  for compatibility reasons this is supported, to avoid any possible overhead
 *  [onDestroyView] is called once per view lifecycle, dropping reference to [UiBinder] since it can
 *  potentially hold references to view
 *  [onDestroy] is called once per lifecycle of whole screen, usually means user is leaving the screen
 *  for good and every resource should be cleaned up.
 *
 *  Platform specific lifecycle adapters must adhere to these rules:
 *
 *                         (view recreated)
 *           |-----<-----<-----<-----<-----<-----<-----|    (screen is
 *           V                                         |     closed)
 * [onCreateView] -> [onStart] < - > [onStop] -> [onDestroyView] -> [onDestroy]
 *
 *
 * View could be recreated in cases including, but not limited to:
 * Android - Configuration changes(screen orientation, locale, etc.)
 * iOS - On view = nil in `didReceiveMemoryWarning`
 */
interface PresentationController<ViewT : Any> {
    /**
     * View is created, [binder] init block contains init for the view
     * and is created alongside here
     * Android:
     * `onCreateView` - Fragment, Conductor
     * iOS:
     * `loadView`
     */
    fun onCreateView(viewT: ViewT)

    /**
     * View is ready to receive new ViewModels, starting binding
     * [onStart] can be triggered after [onCreateView] or after [onStop] (as seen on diagram above)
     * In [onCreateView] case we always bind a model
     * In [onStop] we skip binding if model and oldModel are referentially identical
     * Android:
     * `onAttach` - Conductor
     * `onStart` - Fragment
     * iOS:
     * `viewWillAppear`
     **/
    fun onStart()
    /**
     * View is not ready to receive new ViewModels
     * [binder] is standing by, but [presenter] is still active
     * Android:
     * `onStop` - Fragment
     * `onDetach` - Conductor
     * iOS:
     * `viewDidDisappear`
     **/
    fun onStop()

    /**
     * View is being destroyed
     * [binder] is no longer needed and will be recreated along with the view
     * Android:
     * `onDestroyView` - Fragment,Conductor
     * iOS:
     * `didReceiveMemoryWarning` after `viewDidDisappear` - you can set view prop
     * to nil at this point to conserve memory
     */
    fun onDestroyView()

    /**
     * Screen that depends on this presenter is no longer in use
     * Presenter job is cancelled and presenter references are dropped
     * Android:
     * Fragment - onDestroy && isRemovingCompat
     * fun Fragment.isRemovingCompat() = isRemoving || anyParentIsRemoving()
    fun Fragment.anyParentIsRemoving(): Boolean = parentFragment?.let { it.anyParentIsRemoving()
    || it.isRemoving } ?: false
     * Conductor - onDestroy
     * iOS:
     * Maps to `deinit` on iOS
     **/
    fun onDestroy()
}


class PresentationControllerImpl<StateT : Any, ViewModelT : Any, EventT : Any, ViewT : Any>(
        private val presenter: Presenter<ViewModelT, EventT, StateT>,
        private val binderLambda: (ViewT) -> UiBinder<ViewModelT>
) : CoroutineScope by CoroutineScope(context = Dispatchers.Default + SupervisorJob()), PresentationController<ViewT> {

    private val presentation = CoroutineScope(Dispatchers.Default + SupervisorJob()).launch { presenter.start() }


    /**
     * [binder] is nullable because binders can have explicit or implicit references to a view,
     * and is recreated with the view to avoid any accidental memory leaks
     */
    private var binder: UiBinder<ViewModelT>? = null


    /**
     * [viewJustCreated] is a flag that indicates that view was just created
     * in which case a model that could've been bound before is bound again.
     */
    private var viewJustCreated: Boolean = false

    override fun onCreateView(viewT: ViewT) {
        this.binder = binderLambda(viewT)
        viewJustCreated = true
    }

    private var oldModel: ViewModelT? = null

    override fun onStart() {
        launch(Dispatchers.Unconfined) {
            presenter.models.flowOn(Dispatchers.Main).collect { model ->
                if (oldModel === model && viewJustCreated.not())
                    return@collect//view is already bound to this model
                requireNotNull(binder, { "Lifecycle violation, [onCreateView] must be called before [onStart]" })
                        .bind(model, oldModel)
                oldModel = model
                withContext(Dispatchers.Main) {
                    viewJustCreated = false
                }
            }
        }

    }


    override fun onStop() {
        coroutineContext.cancelChildren()
    }


    override fun onDestroyView() {
        binder = null
    }


    override fun onDestroy() {
        presentation.cancel()
    }


}


