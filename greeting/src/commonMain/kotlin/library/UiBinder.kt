package library

/**
 * Binds models to views, models are provided by presenter flow.
 * It's safe to store view references inside inheritors, reference to binder is
 * dropped at the same time as the view is being destroyed
 */
interface UiBinder<ViewModelT : Any> {
    fun bind(model: ViewModelT, oldModel: ViewModelT?)
}