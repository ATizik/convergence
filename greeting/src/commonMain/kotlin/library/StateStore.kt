package org.greeting.library

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

/**
 * This is a container class around the actual state itself. It has a few optimizations to ensure
 * safe usage of state.
 *
 * All state reducers are run in a coroutine, backed by a channel to ensure that they don't have race
 * conditions with each other.
 *
 */
interface IStateStore<S : Any> : Disposable {
    val state: S
    fun get(block: (S) -> Unit)
    fun set(stateReducer: S.() -> S)
    val flow: Flow<S>
}


internal open class StateStore<S : Any>(
        initialState: S,
        final override val coroutineContext: CoroutineContext = Dispatchers.Default)
    : Disposable, CoroutineScope, IStateStore<S> {

    /**
     * The stateChannel is where state changes should be pushed to.
     */
    private val stateChannel = ConflatedBroadcastChannel(initialState)

    /**
     * The flow observes the stateChannel but only emits events when the state actually changed.
     */
    override val flow: Flow<S> = stateChannel.asFlow()
    /**
     * This is automatically updated from a subscription on the stateChannel for easy access to the
     * current state.
     */
    override val state: S
        get() = stateChannel.value

    sealed class Job<S> {
        class SetQueueElement<S>(val reducer: S.() -> S) : Job<S>()
        class GetQueueElement<S>(val block: (S) -> Unit) : Job<S>()
    }

    /**
     * Coroutine responsible for processing get and set blocks. Sequentially processes every block, [Job.SetQueueElement] in higher priority.
     * Once every [Job.SetQueueElement] is processed, coroutine iterates over [Job.GetQueueElement]s until no elements are left, or a new message
     * is sent to an [queueChannel]
     */
    private val queueChannel = Channel<Job<S>>(capacity = Channel.UNLIMITED)
    private val actor = launch {

        val getQueue = mutableListOf<(S) -> Unit>()
        queueChannel.consumeEach { msg ->
            try {
                when (msg) {
                    is Job.GetQueueElement<S> -> getQueue += msg.block

                    is Job.SetQueueElement<S> -> stateChannel.value
                            .let { msg.reducer(it) }
                            .let(stateChannel::offer)
                }

                while (queueChannel.isEmpty) {
                    stateChannel.value.let(
                            getQueue.getOrNull(0)
                                    ?.also { getQueue.removeAt(0) }
                                    ?: break)
                }

            } catch (t: Throwable) {
                handleError(t)
            }
        }
    }


    /**
     * Get the current state. The block of code is posted to a queue and all pending [set] blocks
     * are guaranteed to run before the get block is run.
     */
    override fun get(block: (S) -> Unit) {
        queueChannel.offer(Job.GetQueueElement(block))
    }


    /**
     * Call this to update the state. The state reducer will get added to a queue that is processed
     * on a specified dispatcher. The state reducer's receiver type is the current state when the
     * reducer is called.
     *
     * An example of a reducer would be `{ copy(myProperty = 5) }`. The copy comes from the copy
     * function on a Kotlin data class and can be called directly because state is the receiver type
     * of the reducer. In this case, it will also implicitly return the only expression so that is
     * all of the code required.
     */
    override fun set(stateReducer: S.() -> S) {
        queueChannel.offer(Job.SetQueueElement(stateReducer))
    }

    private fun handleError(throwable: Throwable) {
        // Throw the root cause to remove all of the rx stacks.
        // TODO: better error handling, maybe error stream?
        var e: Throwable? = throwable
        while (e?.cause != null) e = e.cause
        e?.let { throw it }
    }

    override fun isDisposed() = coroutineContext.isActive

    override fun dispose() {
        coroutineContext.cancel()
    }

}


suspend fun <S : Any> IStateStore<S>.get() = suspendCancellableCoroutine<S> { cont ->
    get { cont.resume(it) }
}

fun <S : Any> IStateStore<S>.set(state: S) = set { state }

interface Disposable {
    fun isDisposed(): Boolean
    fun dispose()
}