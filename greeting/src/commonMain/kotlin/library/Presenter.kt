package org.greeting

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.toChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flowViaChannel
import org.greeting.library.IStateStore
import org.greeting.library.StateStore
import kotlin.coroutines.CoroutineContext

/**
 * @property models - flow of classical view models that should be bound as directly as possible
 * without any additional transformations
 * @property events - flow of events - user actions, platform events etc.
 * @property stateStore - Redux style state store
 * The usual flow of data looks like this:
 *
 *              presenter
 *                  |
 *                  ⌄
 * [events] -> [stateStore] -> [models]
 *     ⌃                           |
 *     |                           ⌄
 *     - - - < - - - < - - - < - - -
 *
 * [models] bound to ui are represented on the screen and are a source of [events] (e.g. click listeners)
 * Every event is a sealed class inheritor, exhaustive when transforms event to concrete [StateT]
 * which is passed to [stateStore], every operation is synchronized there, see [StateStore] implementation.
 * [StateStore.flow] is a flow of states, which are again transformed, now to classical view models - usually
 * they contain click listener lambdas and data that is bound directly without modifications on native platforn
 * side. It's preferable to use expect/actual transformations inside presenter to represent differences between
 * ui - that way we still stay polymorphic and do control inversion even if ui has different representations
 */
interface Presenter<ViewModelT : Any, EventT : Any, StateT : Any> : CoroutineScope {
    val models: Flow<ViewModelT>
    val events: Flow<EventT>
    val stateStore: IStateStore<StateT>
    fun offerEvent(eventT: EventT)
    fun offerModel(modelT: ViewModelT)
    suspend fun start()
}

class PresenterImpl<ViewModelT : Any, EventT : Any, StateT : Any>
(initState: StateT,
 exceptionHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable -> },//TODO: Log errors
 override val coroutineContext: CoroutineContext = Dispatchers.Default + SupervisorJob() + exceptionHandler)
    : Presenter<ViewModelT, EventT, StateT> {

    override fun offerModel(modelT: ViewModelT) {
        _models.offer(modelT)
    }

    override fun offerEvent(eventT: EventT) {
        _events.offer(eventT)
    }

    private val _models = ConflatedBroadcastChannel<ViewModelT>()
    override val models: Flow<ViewModelT> = _models.asFlow()

    private val _events = Channel<EventT>(Channel.RENDEZVOUS)
    override val events: Flow<EventT> = flowViaChannel(Channel.RENDEZVOUS) { chan ->
        _events.toChannel(chan)
    }

    override val stateStore: IStateStore<StateT> = StateStore(initState, coroutineContext)

    override suspend fun start() {
        //This class is supposed to be used as delegate, so this override is dud
    }
}