package org.greeting.sample

import com.database.DbImpl
import sample.Database
import sample.schema._repo

class RepositoriesRepository(private val db: DbImpl = Database) {
    fun getAll() = db.repoQueries.selectAll().asFlow().mapList(_repo::toDomain)
    fun save(repo: MainState.Repo) {
        db.repoQueries.save(repo.id, repo.name, repo.description)
    }

    fun remove(id: Long) {
        db.repoQueries.remove(id)
    }
}

fun _repo.toDomain() = MainState.Repo(repo_id, name, description)