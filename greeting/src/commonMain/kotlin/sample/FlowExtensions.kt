package org.greeting.sample

import com.squareup.sqldelight.Query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.CONFLATED
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowWith
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlin.coroutines.CoroutineContext
import kotlin.jvm.JvmName
import kotlin.jvm.JvmOverloads

/** Turns this [Query] into a [Flow] which emits whenever the underlying result set changes. */
@FlowPreview
@JvmName("toFlow")
fun <T : Any> Query<T>.asFlow(): Flow<Query<T>> = flow {
    val channel = Channel<Unit>(CONFLATED)
    val listener = object : Query.Listener {
        override fun queryResultsChanged() {
            channel.offer(Unit)
        }
    }
    addListener(listener)
    try {
        emit(this@asFlow)
        for (item in channel) {
            emit(this@asFlow)
        }
    } finally {
        removeListener(listener)
    }
}

@FlowPreview
@JvmOverloads
fun <T : Any> Flow<Query<T>>.mapToOne(
        context: CoroutineContext = Dispatchers.Default
): Flow<T> {
    return flowWith(context) {
        map { it.executeAsOne() }
    }
}

@FlowPreview
@JvmOverloads
fun <T : Any> Flow<Query<T>>.mapToOneOrDefault(
        defaultValue: T,
        context: CoroutineContext = Dispatchers.Default
): Flow<T> {
    return flowWith(context) {
        map { it.executeAsOneOrNull() ?: defaultValue }
    }
}

@FlowPreview
@JvmOverloads
fun <T : Any> Flow<Query<T>>.mapToOneOrNull(
        context: CoroutineContext = Dispatchers.Default
): Flow<T?> {
    return flowWith(context) {
        map { it.executeAsOneOrNull() }
    }
}

@FlowPreview
@JvmOverloads
fun <T : Any> Flow<Query<T>>.mapToOneNonNull(
        context: CoroutineContext = Dispatchers.Default
): Flow<T> {
    return flowWith(context) {
        mapNotNull { it.executeAsOneOrNull() }
    }
}

@FlowPreview
@JvmOverloads
fun <T: Any> Flow<Query<T>>.mapToList(
        context: CoroutineContext = Dispatchers.Default
): Flow<List<T>> {
    return flowWith(context) {
        map { it.executeAsList() }
    }
}

inline fun <A : Any, B> Flow<Query<A>>.mapList(crossinline mapper: A.() -> B) = mapToList().map { it.map(mapper) }