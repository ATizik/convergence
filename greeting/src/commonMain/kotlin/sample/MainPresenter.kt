package org.greeting.sample

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import org.greeting.Presenter
import org.greeting.PresenterImpl
import sample.ApplicationApi


sealed class MainItems {
    class SimpleText(val text: String, val id: Long) : MainItems()
    class ClickableItem(val header: String, val id: Long, val onClick: () -> Unit) : MainItems()
    class Image(val image: ImageUrl, val id: Long) : MainItems()
}

data class MainScreenState(
        val state: MainState,
        val localList: List<MainState.Repo>
)

sealed class MainState {
    object Init : MainState()
    object Loading : MainState()
    object Error : MainState()
    data class Repositories(val list: List<Repo>) : MainState()


    class Repo(val id: Long, val name: String, val description: String)
}


sealed class MainEvents {
    object OnTakeMeBackClicked : MainEvents()
    object OnLoadingClicked : MainEvents()
    class SaveRepository(val repo: MainState.Repo) : MainEvents()
    class RemoveRepository(val id: Long) : MainEvents()
}

inline class ImageUrl(val url: String)

class MainPresenter(
        private val applicationApi: ApplicationApi = ApplicationApi(),
        private val repositoriesRepository: RepositoriesRepository = RepositoriesRepository()
) : Presenter<List<MainItems>, MainEvents,
        MainScreenState> by PresenterImpl(MainScreenState(MainState.Init, listOf())) {

    override suspend fun start() {
        launch {
            events.retry().collect { event ->
                @Suppress("IMPLICIT_CAST_TO_ANY")//
                when (event) {
                    MainEvents.OnLoadingClicked -> {
                        stateStore.set { copy(state = MainState.Loading) }
                        runCatching { applicationApi.gitList() }
                                .onSuccess { rsp -> stateStore.set { copy(state = MainState.Repositories(rsp.map { MainState.Repo(it.id, it.name, it.description) })) } }
                                .onFailure { stateStore.set { copy(state = MainState.Error) } }
                    }

                    is MainEvents.OnTakeMeBackClicked -> stateStore.set { copy(state = MainState.Init) }

                    is MainEvents.SaveRepository -> stateStore.get {
                        repositoriesRepository.save(event.repo)
                    }

                    is MainEvents.RemoveRepository -> repositoriesRepository.remove(event.id)

                }.let { }
            }
        }


        launch {
            repositoriesRepository.getAll().collect { stateStore.set { copy(localList = it) } }
        }

        stateStore.flow.collect {
            when (it.state) {
                is MainState.Init ->
                    initSection() + savedSection(it)

                is MainState.Loading -> listOf(MainItems.SimpleText("LOADING", 1),
                        MainItems.Image(ImageUrl("https://cdn.iconscout.com/icon/free/png-256/kotlin-283155.png"), 13),
                        MainItems.SimpleText("LOADING", 3))

                is MainState.Repositories -> listOf(MainItems.ClickableItem("Behold the power of kotlin! Click me, by the way", 100) {
                    offerEvent(MainEvents.OnTakeMeBackClicked)
                }) + it.state.list.flatMap { repoToItems(it, true) }

                is MainState.Error -> {
                    listOf(MainItems.ClickableItem("ERROR! EVERYTHING IS LOST! Try again though(CLICK ME)", 111) {
                        offerEvent(MainEvents.OnLoadingClicked)
                    })
                }
            }.let(::offerModel)

            if (it.state is MainState.Loading) {
                delay(1000)
            }

        }

    }

    private fun savedSection(it: MainScreenState) =
            it.localList.flatMap { repoToItems(it, false) }



    private fun repoToItems(it: MainState.Repo, save: Boolean = true): List<MainItems> =
            listOf(MainItems.SimpleText(it.name, it.id * 10),
                    MainItems.SimpleText(it.description, it.id * 10 + 1),
                    if (save) MainItems.ClickableItem("CLICK TO SAVE", it.id * 10 + 2) {
                        offerEvent(MainEvents.SaveRepository(it))
                    } else MainItems.ClickableItem("CLICK TO REMOVE", +it.id * 10 + 3) {
                        offerEvent(MainEvents.RemoveRepository(it.id))
                    },
                    MainItems.Image(ImageUrl("https://cdn.pixabay.com/photo/2018/02/19/20/28/abstract-3166168_1280.png")
                            , it.id * 10 + 4)
            )


}

expect fun MainPresenter.initSection():List<MainItems>

