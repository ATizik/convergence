package sample

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.http.Url
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.list


class ApplicationApi {
    companion object val client = HttpClient() {
        install(JsonFeature) {
            serializer = KotlinxSerializer(Json.nonstrict).apply {
                register(Git.serializer().list)
            }
        }
    }

    suspend fun gitList() = client.get<List<Git>>(Url("https://api.github.com/orgs/octokit/repos"))

}

@Serializable
data class Git(
        val id: Long,
        val name: String,
        val description: String

)


