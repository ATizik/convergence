package sample

import com.database.DbImpl
import com.squareup.sqldelight.db.SqlDriver

expect val driver: SqlDriver
expect val Database: DbImpl
fun createQueryWrapper(driver: SqlDriver) = DbImpl(driver = driver)


object Schema : SqlDriver.Schema by DbImpl.Schema