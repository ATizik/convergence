package sample

import com.database.DbImpl
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.ios.NativeSqliteDriver


actual val driver: SqlDriver = NativeSqliteDriver(DbImpl.Schema, "app.db")
actual val Database: DbImpl = createQueryWrapper(driver)